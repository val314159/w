;;(define-key key-translation-map [?\C-h] [?\C-?])
;;(define-key key-translation-map [?\M-h] [?\M-\d])

(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)

(menu-bar-mode (if (display-graphic-p) t -1))

(defun make ()
  (interactive)
  (save-buffer)
;  (compile "makex"))
  (compile "/usr/local/bin/makex"))
(global-set-key (kbd "C-z") 'make)
