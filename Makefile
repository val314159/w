V?=.v
P?=-p3.9
build:: .v views
views::
	$V/bin/python -um w.filter public $@ -tw
serve:: build
	$V/bin/bottle.py w -sgevent --debug --reload -b:80
.v:
	virtualenv $P $V
	$V/bin/pip install -r requirements.txt
realclean:: _realclean _clean _tree
clean::                _clean _tree
_tree:: ; tree -I .git -I .v -I __pycache__ -a
_clean::
	find . -name .\*~ -o -name \*~ | xargs rm -fr
	rm -fr views
_realclean::
	find . -name __pycache__ | xargs rm -fr
	rm -fr .emacs.d/auto-save-list .v

