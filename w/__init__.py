import os,sys,bottle;app=bottle.app()
@app.get(            '/_<name>.html')
@app.get('/<path:path>/_<name>.html')
def _(path='.', name='index', ext='html'):
    raise bottle.HTTPError(404)
@app.get(            '/')
@app.get('/<path:path>/')
@app.get(            '/<name>.html')
@app.get('/<path:path>/<name>.html')
def _(path='.', name='index', ext='html'):
    fullpath = f'{path}/{name}.{ext}'
    return bottle.mako_template(fullpath)
@app.get(            '/<name>.<ext>')
@app.get('/<path:path>/<name>.<ext>')
def _(path='.', name='index', ext='html'):
    fullpath = f'{path}/{name}.{ext}'
    return bottle.static_file(fullpath, 'views')
@app.get('<path:path>')
def _(path):
    raise bottle.redirect(path+'/')
