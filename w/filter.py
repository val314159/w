#!/usr/bin/python3
import os, sys
try   : src = sys.argv[1]
except: src = 'public'
try   : dst = sys.argv[2]
except: dst = 'views'
for s, _, _ in os.walk(src):
    d = s.replace(src,dst,1)
    assert 0==os.system('mkdir -p '+d)
    for f in _:
        cmd, S, D = 'cp', s+'/'+f, d+'/'+f
        if f.endswith('.pug'):
            cmd = '.v/bin/pypugjs -c mako -e html'
            D = D[:-3] + 'html'
            pass
        assert 0==os.system(f'{cmd} {S} {D}')
        pass
    pass
if '-tw' in sys.argv:
    assert 0==os.system(f'tailwind -o {dst}/site.css')
